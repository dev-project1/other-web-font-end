"use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gCoursesDB = {
      description: "This DB includes all courses in system",
      courses: [
        {
          id: 1,
          courseCode: "FE_WEB_ANGULAR_101",
          courseName: "How to easily create a website with Angular",
          price: 750,
          discountPrice: 600,
          duration: "3h 56m",
          level: "Beginner",
          coverImage: "images/courses/course-angular.jpg",
          teacherName: "Morris Mccoy",
          teacherPhoto: "images/teacher/morris_mccoy.jpg",
          isPopular: false,
          isTrending: true
        },
        {
          id: 2,
          courseCode: "BE_WEB_PYTHON_301",
          courseName: "The Python Course: build web application",
          price: 1050,
          discountPrice: 900,
          duration: "4h 30m",
          level: "Advanced",
          coverImage: "images/courses/course-python.jpg",
          teacherName: "Claire Robertson",
          teacherPhoto: "images/teacher/claire_robertson.jpg",
          isPopular: false,
          isTrending: true
        },
        {
          id: 5,
          courseCode: "FE_WEB_GRAPHQL_104",
          courseName: "GraphQL: introduction to graphQL for beginners",
          price: 850,
          discountPrice: 650,
          duration: "2h 15m",
          level: "Intermediate",
          coverImage: "images/courses/course-graphql.jpg",
          teacherName: "Ted Hawkins",
          teacherPhoto: "images/teacher/ted_hawkins.jpg",
          isPopular: true,
          isTrending: false
        },
        {
          id: 6,
          courseCode: "FE_WEB_JS_210",
          courseName: "Getting Started with JavaScript",
          price: 550,
          discountPrice: 300,
          duration: "3h 34m",
          level: "Beginner",
          coverImage: "images/courses/course-javascript.jpg",
          teacherName: "Ted Hawkins",
          teacherPhoto: "images/teacher/ted_hawkins.jpg",
          isPopular: true,
          isTrending: true
        },
        {
          id: 8,
          courseCode: "FE_WEB_CSS_111",
          courseName: "CSS: ultimate CSS course from beginner to advanced",
          price: 750,
          discountPrice: 600,
          duration: "3h 56m",
          level: "Beginner",
          coverImage: "images/courses/course-javascript.jpg",
          teacherName: "Juanita Bell",
          teacherPhoto: "images/teacher/juanita_bell.jpg",
          isPopular: true,
          isTrending: true
        },
        {
          id: 14,
          courseCode: "FE_WEB_WORDPRESS_111",
          courseName: "Complete Wordpress themes & plugins",
          price: 1050,
          discountPrice: 900,
          duration: "4h 30m",
          level: "Advanced",
          coverImage: "images/courses/course-wordpress.jpg",
          teacherName: "Clevaio Simon",
          teacherPhoto: "images/teacher/clevaio_simon.jpg",
          isPopular: true,
          isTrending: false
        }
      ]
    }

    // Biến toàn cục để lưu trữ id voucher đang đc update or delete. Mặc định = 0;
    var gRowIdClick = 0;

    var gColName = ["id", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action"]
    var gID_COL = 0;
    var gCOURSE_CODE_COL = 1;
    var gCOURSE_NAME_COL = 2;
    var gPRICE_COL = 3;
    var gDISCOUNT_COL = 4;
    var gDURATION_COL = 5;
    var gLEVEL_COL = 6;
    var gCOVER_IMG_COL = 7;
    var gTEACHER_NAME_COL = 8;
    var gTEACHER_PHOTO_COL = 9;
    var gPOPULAR_COL = 10;
    var gTRENDING_COL = 11;
    var gACTION_COL = 12;

    $("#course-list").DataTable({
      columns: [
        { data: gColName[gID_COL] },
        { data: gColName[gCOURSE_CODE_COL] },
        { data: gColName[gCOURSE_NAME_COL] },
        { data: gColName[gPRICE_COL] },
        { data: gColName[gDISCOUNT_COL] },
        { data: gColName[gDURATION_COL] },
        { data: gColName[gLEVEL_COL] },
        { data: gColName[gCOVER_IMG_COL] },
        { data: gColName[gTEACHER_NAME_COL] },
        { data: gColName[gTEACHER_PHOTO_COL] },
        { data: gColName[gPOPULAR_COL] },
        { data: gColName[gTRENDING_COL] },
        { data: gColName[gACTION_COL] },
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `
            <button class="btn btn-link btn-edit-course" data-toggle="tooltip" data-placement="bottom" title="Update"><i class="far fa-edit"></i></button>
            <button class="btn btn-link btn-sm btn-delete-course" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>`
        },
        {
          targets: gCOVER_IMG_COL,
          render: changeCourseImgPicture
        },
        {
          targets: gTEACHER_PHOTO_COL,
          render: changeTeacherPicture
        }
      ]
    })
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();
    $(".btn-create-course").on("click", function () {
      onBtnAddCourseClick();
    })
    // thêm event cho nút Create Course
    $("#create-course-modal").on("click", "#btn-create-course-modal", function () {
      onBtnCreateCourseModalClick();
    })

    // thêm sự kiện cho nút edit 
    $("#course-list").on("click", ".btn-edit-course", function () {
      onBtntEditCourseClick(this);
    })

    // thêm sự kiện cho nút delete
    $("#course-list").on("click", ".btn-delete-course", function () {
      onBtnDeleteCourseClick(this);
    })

    //thêm sk cho các nút trên modal
    // thêm sk cho nút Update Course
    $("#update-course-modal").on("click", "#btn-update-course-modal", function () {
      onBtnUpdateCourseModalClick()
    })

    // thêm sk cho nút Delte Course
    $("#delete-confirm-modal").on("click", "#btn-confirm-delete-course", function () {
      onBtnDeteleCourseModalClick()
    })



    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
      console.log("A")
      loadCourseList(gCoursesDB.courses);
    }
    // hàm xử lý khi ẫn nút Thêm
    function onBtnAddCourseClick() {
      $("#create-course-modal").modal("show");
    }
    // hàm xử lý khi ần nút Create trên Modal
    function onBtnCreateCourseModalClick() {
      console.log("B")
      // khai báo đối tượng chứa course data
      var vNewCourseObj = {
        id: 0,
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "0",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: null,
        isTrending: null
      }
      // b1: thu thập dl
      getNewCourseDataToObj(vNewCourseObj);
      // b2: validate dl
      var vIsValid = validateNewCourseDataToObj(vNewCourseObj);
      if (vIsValid) {
        debugger;
        // thêm vNewCourseObj vào gCoursesDB
        gCoursesDB.courses.push(vNewCourseObj);
        // thông báo thêm Course thành công
        alert("Bạn đã thêm Course thành công");
        // reset modal
        resetAddCourseModal();
        // load lại data trên web 
        loadCourseList(gCoursesDB.courses);
        // ẩn modal
        $("#create-course-modal").modal("hide");
      }
    }

    // hàm xử lý khi ấn nút edit
    function onBtntEditCourseClick(paramElement) {
      // thu thập dl trên dòng
      var vRowDataObj = getDataRowClick(paramElement);
      console.log(vRowDataObj);
      // hiện modal edit lên
      $("#update-course-modal").modal("show");
      // add dl vào modal
      addDataToUpdateModal(vRowDataObj);
    }

    // hàm xl khi ấn nút update trên modal
    function onBtnUpdateCourseModalClick() {
      // khai báo đối tượng chứa data update
      var vUpdateCourseObj = {
        id: 1,
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: null,
        isTrending: null
      }
      // b1: thu thập dl
      getDataToUpdateObj(vUpdateCourseObj);
      // b2: kiểm tra dl
      var vIsValid = validateUpdateObj(vUpdateCourseObj);
      if (vIsValid) {
        // B3: update course
        updateCourse(vUpdateCourseObj);
        //console.log(gCoursesDB.courses);
        // B4: xử lý front-end
        alert("Sửa course thành công!");
        loadCourseList(gCoursesDB.courses);
        resertUpdateCourseModal();
        $("#update-course-modal").modal("hide");

      }
    }

    // hàm xl khi ấn nút delete
    function onBtnDeleteCourseClick(paramElement) {
      // thu thập dl trên dòng
      var vRowDataObj = getDataRowClick(paramElement);
      console.log(gRowIdClick);
      // hiện modal delete lên
      $("#delete-confirm-modal").modal("show");
    }

    // hàm xử lý sự kiện confirm delete voucher trên modal click
    function onBtnDeteleCourseModalClick() {
      // B1: thu thập dữ liệu (ko có)
      // B2: validate (ko có)
      // B3: xóa voucher
      deleteCourse(gRowIdClick);
      // B4: hiển thị dữ liệu lên front-end (load lại table, ẩn modal)
      alert("Xóa course thành công!");
      loadCourseList(gCoursesDB.courses);
      // ẩn modal confirm xóa đi
      $("#delete-confirm-modal").modal("hide");

    }


    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm hiển thị course ra web
    function loadCourseList(paramCourseObj) {
      var vTable = $("#course-list").DataTable();
      vTable.clear();
      vTable.rows.add(paramCourseObj);
      vTable.draw();
    }
    // hàm thu thập dl trên create modal
    function getNewCourseDataToObj(paramCourseObj) {
      paramCourseObj.id = getNextId();
      paramCourseObj.courseCode = $("#input-create-course-code").val().trim();
      paramCourseObj.courseName = $("#input-create-course-name").val().trim();
      paramCourseObj.price = parseInt($("#input-create-price").val().trim());
      paramCourseObj.discountPrice = parseInt($("#input-create-discount-price").val().trim());
      paramCourseObj.duration = $("#input-create-duration").val().trim();
      paramCourseObj.level = $("#select-level").val();
      paramCourseObj.coverImage = $("#input-create-cover-image").val().trim();
      paramCourseObj.teacherName = $("#input-create-teacher-name").val().trim();
      paramCourseObj.teacherPhoto = $("#input-create-teacher-photo").val().trim();
      paramCourseObj.isPopular = $("#select-popular").val();
      paramCourseObj.isTrending = $("#select-trending").val();
    }
    // hàm lấy ra đc id voucher tiếp theo, dùng khi thêm mới voucher
    function getNextId() {
      var vNextId = 0;
      // nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
      if (gCoursesDB.courses.length == 0) {
        vNextId = 1;
      }
      else { // id tiếp theo bằng id của phần tử cuối cùng cộng thêm 1
        vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
      }
      return vNextId;
    }
    // hàm kiểm tra dl khi create course
    function validateNewCourseDataToObj(paramCourseObj) {
      if (paramCourseObj.courseCode === "") {
        alert("Không được để trống Course Code");
        return false;
      }
      // kiểm tra tính ! của courseCode
      var vCourseCodeCheck = getCourseByCourseCode(paramCourseObj.courseCode);
      if(vCourseCodeCheck != null){
        alert("Không được trùng lặp Course Code");
        return false;
      }
      if (paramCourseObj.courseName === "") {
        alert("Không được để trống Course Name");
        return false;
      }
      if (isNaN(paramCourseObj.price)) {
        alert("Cần nhập dl price dạng số");
        return false;
      }
      if(paramCourseObj.price < 1){
        alert("Cần nhập dl price là số dương");
        return false;
      }
      if (isNaN(paramCourseObj.discountPrice)) {
        alert("Cần nhập dl discount dạng số");
        return false;
      }
      if (paramCourseObj.discountPrice < 0) {
        alert("Cần nhập dl discount là số lớn hơn 0");
        return false;
      }
      if (paramCourseObj.duration === "") {
        alert("Không được để trống Duration");
        return false;
      }
      if (paramCourseObj.level === "null") {
        alert("Bạn cần chọn Level");
        return false;
      }
      if (paramCourseObj.coverImage === "") {
        alert("Không được để trống ảnh nền khóa học");
        return false;
      }
      if (paramCourseObj.teacherName === "") {
        alert("Không được để trống Teacher Name");
        return false;
      }
      if (paramCourseObj.teacherPhoto === "") {
        alert("Không được để trống Teacher Photo");
        return false;
      }
      if (paramCourseObj.isPopular === "null") {
        alert("Bạn cần chọn Popular");
        return false;
      }
      if (paramCourseObj.isTrending === "null") {
        alert("Bạn cần chọn Trending");
        return false;
      }
      return true;
    }
    // hàm reset xóa trắng modal
    function resetAddCourseModal() {
      $("#input-create-course-code").val("");
      $("#input-create-course-name").val("");
      $("#input-create-price").val("");
      $("#input-create-discount-price").val("");
      $("#input-create-duration").val("");
      $("#select-level").val("null");
      $("#input-create-cover-image").val("");
      $("#input-create-teacher-name").val("");
      $("#input-create-teacher-photo").val("");
      $("#select-popular").val("null");
      $("#select-trending").val("null");
    }
    // hàm thay text bằng img course
    function changeCourseImgPicture(paramData) {
      for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        if (paramData === gCoursesDB.courses[bI].coverImage) {
          paramData = "<img src ='" + gCoursesDB.courses[bI].coverImage + "' width='60' height='40'>";
          return paramData;
        }
      }
    }
    // hàm thay text bằng img teacher
    function changeTeacherPicture(paramData) {
      for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        if (paramData === gCoursesDB.courses[bI].teacherPhoto) {
          paramData = "<img src ='" + gCoursesDB.courses[bI].teacherPhoto + "' width='40' height='40'>";
          return paramData;
        }
      }

    }

    // hàm thu thập dl trên dòng khi click btn trên dòng
    function getDataRowClick(paramClick) {
      var vRowClick = $(paramClick).parents("tr");
      var vTable = $("#course-list").DataTable();
      var vRowData = vTable.row(vRowClick).data();
      gRowIdClick = vRowData.id;
      return vRowData;
    }

    // hàm add dl vào modal
    function addDataToUpdateModal(paramCourseObj) {
      $("#input-update-course-code").val(paramCourseObj.courseCode);
      $("#input-update-course-name").val(paramCourseObj.courseName);
      $("#input-update-price").val(paramCourseObj.price);
      $("#input-update-discount-price").val(paramCourseObj.discountPrice);
      $("#input-update-duration").val(paramCourseObj.duration);
      $("#select-update-level").val(paramCourseObj.level);
      $("#input-update-cover-image").val(paramCourseObj.coverImage);
      $("#input-update-teacher-name").val(paramCourseObj.teacherName);
      $("#input-update-teacher-photo").val(paramCourseObj.teacherPhoto);
      if (paramCourseObj.isPopular == true) {
        $("#select-update-popular").val("true");
      }
      else if (paramCourseObj.isPopular == false) {
        $("#select-update-popular").val("false");
      }
      if (paramCourseObj.isTrending == true) {
        $("#select-update-trending").val("true");
      }
      else if (paramCourseObj.isTrending == false) {
        $("#select-update-trending").val("false");
      }
    }

    // hàm thu thập dl trên modal update
    function getDataToUpdateObj(paramCourseObj) {
      paramCourseObj.id = gRowIdClick;
      paramCourseObj.courseCode = $("#input-update-course-code").val().trim();
      paramCourseObj.courseName = $("#input-update-course-name").val().trim();
      paramCourseObj.price = parseInt($("#input-update-price").val().trim());
      paramCourseObj.discountPrice = parseInt($("#input-update-discount-price").val().trim());
      paramCourseObj.duration = $("#input-update-duration").val().trim();
      paramCourseObj.level = $("#select-update-level").val();
      paramCourseObj.coverImage = $("#input-update-cover-image").val().trim();
      paramCourseObj.teacherName = $("#input-update-teacher-name").val().trim();
      paramCourseObj.teacherPhoto = $("#input-update-teacher-photo").val().trim();
      paramCourseObj.isPopular = $("#select-update-popular").val();
      paramCourseObj.isTrending = $("#select-update-trending").val();
    }

    // hàm kiểm tra dl 
    function validateUpdateObj(paramCourseObj) {
      if (paramCourseObj.courseCode === "") {
        alert("Không được để trống Course Code");
        return false;
      }
      if (paramCourseObj.courseName === "") {
        alert("Không được để trống Course Name");
        return false;
      }
      if (isNaN(paramCourseObj.price)) {
        alert("Cần nhập dl price dạng số");
        return false;
      }
      if(paramCourseObj.price < 1){
        alert("Cần nhập dl price là số dương");
        return false;
      }
      if (isNaN(paramCourseObj.discountPrice)) {
        alert("Cần nhập dl discount dạng số");
        return false;
      }
      if (paramCourseObj.discountPrice < 0) {
        alert("Cần nhập dl discount là số lớn hơn 0");
        return false;
      }
      if (paramCourseObj.duration === "") {
        alert("Không được để trống Duration");
        return false;
      }
      if (paramCourseObj.level === "null") {
        alert("Bạn cần chọn Level");
        return false;
      }
      if (paramCourseObj.coverImage === "") {
        alert("Không được để trống ảnh nền khóa học");
        return false;
      }
      if (paramCourseObj.teacherName === "") {
        alert("Không được để trống Teacher Name");
        return false;
      }
      if (paramCourseObj.teacherPhoto === "") {
        alert("Không được để trống Teacher Photo");
        return false;
      }
      if (paramCourseObj.isPopular === "null") {
        alert("Bạn cần chọn Popular");
        return false;
      }
      if (paramCourseObj.isTrending === "null") {
        alert("Bạn cần chọn Trending");
        return false;
      }
      return true;
    }

    /* get course index from voucher id
  // input: paramcourseId là course Id cần tìm index
  / output: trả về chỉ số (index) trong mảng gCoursesDB.courses */
    function getIndexFormCourseId(paramCourseObj) {
      var vCourseIndex = -1;
      var vCourseFound = false;
      var vLoopIndex = 0;
      while (!vCourseFound && vLoopIndex < gCoursesDB.courses.length) {
        if (gCoursesDB.courses[vLoopIndex].id === paramCourseObj) {
          vCourseIndex = vLoopIndex;
          vCourseFound = true;
        }
        else {
          vLoopIndex++;
        }
      }
      return vCourseIndex;
    }

    // hàm reset Update Modal Form
    function resertUpdateCourseModal() {
      $("#input-update-course-code").val("");
      $("#input-update-course-name").val("");
      $("#input-update-price").val("");
      $("#input-update-discount-price").val("");
      $("#input-update-duration").val("");
      $("#select-update-level").val("");
      $("#input-update-cover-image").val("");
      $("#input-update-teacher-name").val("");
      $("#input-update-teacher-photo").val("");
      $("#select-update-popular").val("");
      $("#select-update-trending").val("");
    }

    // hàm thực hiện update course vào mảng
    function updateCourse(paramCourseObj) {
      var vCourseIndex = getIndexFormCourseId(gRowIdClick);
      gCoursesDB.courses.splice(vCourseIndex, 1, paramCourseObj);
    }

    // hàm xóa course theo id course
    function deleteCourse(paramCourseId) {
      var vCourseIndex = getIndexFormCourseId(paramCourseId);
      gCoursesDB.courses.splice(vCourseIndex, 1);
    }

    // hàm kiểm tra tính duy nhất của má khóa học
    // input: paramCourseCode: mã khóa học điền vào 
    // output: trả về obj chứa mã khóa học
    function getCourseByCourseCode(paramCourseCode) {
      var vCourseCodeResult = null;
      var vCourseFound = false;
      var vLoopIndex = 0;
      while (!vCourseFound && vLoopIndex < gCoursesDB.courses.length) {
        if (gCoursesDB.courses[vLoopIndex].courseCode.toLowerCase() === paramCourseCode.toLowerCase()) {
          vCourseCodeResult = gCoursesDB.courses[vLoopIndex];
          vCourseFound = true;
        }
        else {
          vLoopIndex++;
        }
      }
      return vCourseCodeResult;
    }
